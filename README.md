# Discuz!ᵂ 应用开发指导

## 介绍

### Discuz!ᵂ 与 Discuz! X 应用的区别
Discuz!ᵂ 是基于 Discuz! X3.5 实现的 SaaS 服务，依托于 WitFrame 平台而搭建的云端 Discuz! 版本，站长无需自行购买服务器，无需为服务器安装相应的服务软件，无需人工安装，直接云端通过鼠标点击几下，只需 2 分钟即可立刻使用的 Discuz! 版本。因此，在 Discuz!ᵂ 中的应用完全是在云端运行，站长看不到代码。因此也有效的防止了应用代码的泄露风险，保护了开发者的权益。而这便是与 Discuz! X 版本最大的区别，也是优势。同时 Discuz!ᵂ 还可以设置周期付费，让您的应用可以产生持续性收入。

### Discuz!ᵂ 与 Discuz! 未来版本的关系
只要您的应用兼容了 Discuz!ᵂ，就相当于兼容了 Discuz! 的未来版本，当 Discuz! 未来版本发布后，您的应用可以直接兼容。

### Discuz!ᵂ 应用的安装流程
Discuz!ᵂ 应用的购买、安装流程和 Discuz! X 完全一致，但是不存在应用的下载步骤，因为应用代码已经在云端，无需下载。站长购买后跳回 Discuz! 后台直接安装即可。当插件类应用发布新版的时候，也和使用 Discuz! X 一样站长自行决定是否需要升级，随时都可以人工升级插件版本。但是模板类应用是自动升级的，站长无法干预。Discuz!ᵂ 中不接受扩展类应用。

## 开发要点

您必须先将插件、模板兼容 Discuz! X3.5 及 PHP 8.1 环境。此 Git 为特殊版本的 X3.5，部分功能仅用于应用开发者演示，并无实际功能，因此请勿用于正式运营场合。 

### 插件

#### 准备工作

在 Discuz! 开放平台中你无法创建一个同时兼容 Discuz! X 又兼容 Discuz!ᵂ 的应用分支，您需要为 Discuz!ᵂ 单独创建一个应用的分支，请做好应用的备份。

请留意以下新增函数及常量的用法，对之前应用中的相关代码进行替换。

#### DISCUZ_PLUGIN()

如果您的插件中有类似“/source/plugin/xxx”目录的写法调用插件文件，请将其替换为此全局函数，例如：
```php
require_once DISCUZ_PLUGIN('xxx').'/index.php';
```
涉及前端调用的路径写法是 URI 的一部分，而非后台目录文件调用，无需修改。

友情提示：因安全原因，目前 Discuz!ᵂ 暂时不能完整支持 /source/plugin/ URI 下 PHP 脚本的访问，如非必须请通过 /plugin.php?redirect=identifier:module 入口进行跳转。 

#### DISCUZ_TEMPLATE()

如果您的插件中有类似“/template/xxx”目录的写法调用模板文件，请将其替换为此全局函数，例如：
```php
$f = DISCUZ_TEMPLATE('xxx').'/data.htm';
```

#### DISCUZ_DATA

如果您的插件中有类似“/data”目录的写法读写 /data 目录的文件，请将其替换为 DISCUZ_DATA 常量，例如：
```php
$lock = DISCUZ_DATA.'cache/data.php';
```
由于 Discuz!ᵂ 中所有永久性文件都会存入对象存储，因此 DISCUZ_DATA 目录禁止写入永久文件，否则审核员有权将应用打回。

#### DISCUZ_ROOT_STATIC

由于 /data 目录禁止写入，您可以把永久文件写入到此目录中，它有以下特性：

- 此目录与 DISCUZ_ROOT 意义相同，但只有存放的静态文件（md、txt、js、css、json、xml、图片等）可对外展示；
- 如保存的文件和实际产品文件及路径相同，不会产生覆盖效果，也不会对外展示；
- 此目录占用的空间将计算到 Discuz!ᵂ 用户的平台存储中；

#### 使用对象存储

插件中如涉及上传永久性资源文件，请在所有上传的文件中补充 ftpcmd() 或者使用 discuz_upload() 类处理上传，用 $_G['setting']['attachurl'] 做 URL 前缀拼接下载地址。2 个系统函数在 Discuz!ᵂ 中已经自动兼容了对象存储。

#### 禁止删除文件

Discuz!ᵂ 的插件安装、卸载、升级以及任何脚本中，均不能包含删除插件文件的语句，否则审核员有权将应用打回。

#### XML

您只需保留 UTF-8 格式的 XML 文件。提审之前，请将插件 XML 中的版本兼容 version 项的值改为 “W1.0”，且只有此值。

#### 自定义伪静态

这是 Discuz!ᵂ 中的新增内容，插件可以在安装的时候就可以自定义伪静态规则。

##### 添加伪静态规则

开发者可以在插件的 install.php 中调用方法
```
rewrite_rules(插件标识符, 伪静态规则数组)
```
其他页面可以先判断 rewrite_rules 是否存在，如果不存在，可以引用 function_plugin.php，如：
```php
if(!function_exists('rewrite_rules')){
        include_once libfile('function/plugin');
}
```
完整示例：
```php
if(!function_exists('rewrite_rules')){
        include_once libfile('function/plugin');
}
$rules = array(
    '/list-(\d+)\.html/' => 'forum.php?mod=forumdisplay&fid=\1',
    '/view-(\d+)\.html/' => 'forum.php?mod=viewthread&tid=\1',
);
rewrite_rules('demoplugin', $rules);
```

规则数组按照 PHP 的正则替换格式书写，key 为正则表达式，value 为替换后的 URI。

##### 删除伪静态规则

开发者在站长卸载插件时需要清除伪静态规则，在 uninstall.php 中调用方法，也可以在其他页面使用清除伪静态规则方法
```
rm_rewrite_rules(插件标识符)
```
完整示例：
```php
if(!function_exists('rewrite_rules')){
        include_once libfile('function/plugin');
}
rm_rewrite_rules('demoplugin');
```


#### 多语言

Discuz!ᵂ 原生支持多语言内核，您只需自定义多语言 key 并设置此 key 对应的语言包文件路径即可

##### 设置自定义语言：
```
i18n('set', 'mylang', DISCUZ_ROOT.'./i18n/en');
```
设置多语言 mylang 的语言包文件路径为 DISCUZ_ROOT.'/i18n/en' 目录，支持插件路径

##### 读取设置的自定义语言列表：
```
i18n('get');
```

把设置的语言 key 赋值给 $_G['cookie']['i18n'] 即可实时切换语言

其他第三方插件若要兼容 i18n 对应的语言 key，参考此范例 https://gitee.com/Discuz/DiscuzXPluginSample/tree/master/sample/i18n/mylang


#### 自定义后台菜单

这是 Discuz!ᵂ 中的新增内容，插件无需添加 menu 扩展，即可实现菜单的添加与删除

**您所添加的菜单请务必在卸载插件后清理完毕**

##### 准备工作

如果在非安装、卸载脚本中使用，最好是判断下方法是否存在
示例：
```php
if(!function_exists('set_admin_menu')){
        include_once libfile('function/plugin');
}
```

##### 添加菜单

完整示例：
```php
if(!function_exists('set_admin_menu')){
        include_once libfile('function/plugin');
}
$menus = array(
    array('侧边菜单1','plugins?id=1'),
    array('侧边菜单2','plugins?id=2'),
);
set_admin_menu('顶部菜单名称', $menus);
```

##### 删除菜单

完整示例：
```php
if(!function_exists('remove_admin_menu')){
        include_once libfile('function/plugin');
}
remove_admin_menu('顶部菜单名称');//单独删除
remove_admin_menu(array('顶部菜单名称1','顶部菜单名称2'));//批量删除
```

#### 自定义新平台

这是 Discuz!ᵂ 中的新增内容，开发者可直接添加一个平台管理中心，在这里，您可以完全设计整个后台的菜单以及界面。同时在“站长”->“多平台管理” 站长还能自行调整。

##### XML 格式规范

这是平台配置 XML 文件的模板：

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>
<root>
	<name><![CDATA[新平台]]></name>
	<title><![CDATA[新平台]]></title>
	<framecss><![CDATA[xxx.css]]></framecss>
	<pagecss><![CDATA[ccc.css]]></pagecss>	
	<logo><![CDATA[<a class="logo"><img src="static/image/admincp/logo.svg"></a>]]></logo>
	<navbar><![CDATA[<form></form>]]></navbar>
	<menu>
		<menuId>主菜单1</menuId>
		<sub>
			<subId>action_operation_do1</subId>
			<title>子菜单1</title>
		</sub>
		<sub>
			<subId>action_operation_do2</subId>
			<title>子菜单2</title>
		</sub>
	</menu>
	<menu>
		<menuId>主菜单2</menuId>
		<sub>
			<subId>plugin_id:pmod1</subId>
			<title>子菜单3</title>
		</sub>
		<sub>
			<subId>plugin_id:pmod2</subId>
			<title>子菜单4</title>
		</sub>
	</menu>
	<userdef><![CDATA[1]]></userdef>
</root>
```

- name：平台名称，在多平台列表中展示的名称；
- title：平台网页标题，显示在浏览器标题栏中的名称；
- logo：左上角 LOGO 区域 HTML；
- navbar：右侧搜索区域 HTML；
- framecss：框架页 CSS，可直接写 CSS 代码，也可以写文件 URL (.css 结尾)；
- pagecss：内容页 CSS，规则同 framecss；
- menu：主菜单节点，多个主菜单按照顺序书写；
- menuId：菜单 ID、外显名称，可写语言包名或中文；
- sub 子菜单节点；
- subId：子菜单 ID，格式“action_operation_do”，action、operation、do 为相应页面 GET 参数拼接的内容，opeartion、do 可省略；如果是跳转到插件页面，格式为 “plugin_id:pmod”，id 为插件唯一标识符 ID，pmod 为后台模块的参数，pmod 可省略；
- title：外显名称，可写语言包名或中文；
- type：1=区域开始，2=区域结束；
- showMethod：显示此菜单项的条件，外调方法，系统值不建议修改。插件用 id::method 方式调用具体方法，系统会调用 plugin/id/ 中 class platform_id 的静态方法 method()；
- listMethod：菜单显示内容由具体方法返回，格式同 showMethod；
- subPerms：当前菜单项关联的其他子菜单 ID，多个用逗号“,”分割， ID 格式见 subId；

##### 添加平台

```php
$xml = '<?xml version="1.0" encoding="ISO-8859-1"?>
...
';

menu::platform_add('test', $xml); // test 为平台标识，$xml 为配置数据
```

##### 删除平台

```php
menu::platform_del('test');// test 为平台标识
```

##### platform.class.php 详解

开发者需书写一个 platform.class.php 脚本

plugin/myplugin/platform.class.php 完整示例：

```php
class platform_myplugin {

        // myplugin:showMethodSample
	public static function showMethodSample() {
		return true;
	}        

        // myplugin:listMethodSample
	public static function listMethodSample() {
		return array(
			array('menu_members_edit', 'members_search', 0, '', '', array('members_clean', 'members_repeat')),
			array(cplang('nav_home'), '', 1, 'homestatus'),
			array('menu_maint_doing', 'doing', 0, 'doingstatus'),
			array('menu_maint_share', 'share', 0, 'sharestatus'),
			array(cplang('nav_home'), '', 2, 'homestatus'),
		);
	}
}
```

showMethodSample 为 showMethod 的范例，返回 true/false 即可，true 时菜单项显示
listMethodSample 为 listMethod 的范例，返回的内容会替换掉相应位置中的数据。其中返回的数组与 XML 中 sub 下节点的对应关系为：

0：title  
1：subId  
2：type  
3：showMethod  
4：listMethod  
5：subPerms

#### 判断插件是否试用阶段
工具类插件可能不适用于试用，可以判断是否处于试用阶段，从而给予少量的权限
```php
if(!function_exists('check_addon_trial')){
        include_once libfile('function/plugin');
}
if(check_addon_trial('plugin_identifier')){
    //试用阶段
}
```

### 模板

Discuz!ᵂ 的模板基本上是兼容 Discuz! X 的，您无需做过多修改，尽管如此我们依然提供了完整的 default 模板，请自行对比修改您设计的 Discuz! X 版本模板进行调整。如涉及继承，请根据差异内容进行修改，或者取消继承。尤其注意以下几个方面：

#### 1、聚合首页

聚合首页设置位于后台“论坛”->“聚合首页”，全新的首页可自定义主题列表展示的规则。设置后可配合细胞模板在后台即可进行 DIY 调整。

#### 2、细胞模板

细胞模板为全新的模板元素封装机制，可将复杂的模板内容封装为一个 {cell}、{cells} 标记。其中 {cells} 还支持站长自定义模式，站长在后台可随意 DIY。

##### {cell}

{cell} 为细胞模板的最小元素，可以将模板中的任何内容写于其中，调用方直接写 {cell}。

格式：
```
{cell file}
```

file 为文件名，对应 /template/xxx/cell/ 目录下的文件，支持多级目录，文件扩展名目前固定为 .htm。文件中直接填写“细胞体”内容代码即可。

##### {cells}

{cells} 中可以组合多个 {cell} 元素，可将多个包含 {cell} 的组合再次拼接为一个大的细胞模板。

格式：
```
{cell file}
```

file 为文件名，对应 /template/xxx/cell/ 目录下的文件，支持多级目录。

扩展名为 .htm 的模板文件规则同 {cell}，里面可以包含 {cell}。
扩展名为 .php 的模板文件需要书写成 class 类，具体参考“template/default/cells/forum/portal/”目录下的文件。

#### 3、账号管理

全新的账号管理代替了之前的“密码安全”同时支持多个第三方登录的绑定，此开发者版本此页面演示数据，无具体功能。请开发者自行对照演示数据设计模板。

#### 4、登录

登录界面配合细胞模板增加了第三方登录的图标集，开发者需自行补充相关的细胞模板到自己的模板中。

## 发布 Discuz!ᵂ 应用
登录开发者平台后，在首页点击“添加应用”，按照提示逐步填写应用信息，并上传预览图和文件包.

![输入图片说明](addon/image.png)

### Discuz!ᵂ 支持服务周期设置

为了更好的服务 SaaS 客户，开发者开发的应用现已支持服务周期设置。若开启服务周期设定，Discuz!ᵂ 站点首次购买应用的使用有效期为 12 个月，后续站点若继续使用、更新应用，将需进行续费。建议开发者第二年续费服务周期统一设定为12个月/周期。

![输入图片说明](addon/image2.png)

更多的发布应用方法：https://open.dismall.com/?ac=document&page=faq_addon

## 体验使用 Discuz!ᵂ

前往站点：https://w.discuz.vip

## Discuz! RESTful API 手册

https://gitee.com/Discuz/discuz-restful-api
