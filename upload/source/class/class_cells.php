<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class cells {

	private static $r_dzTpl = array(
		"/[\n\r\t]*\{block\/(\d+?)\}[\n\r\t]*/i",
		"/[\n\r\t]*\{blockdata\/(\d+?)\}[\n\r\t]*/i",
		"/[\n\r\t]*\{ad\/(.+?)\}[\n\r\t]*/i",
		"/[\n\r\t]*\{ad\s+([a-zA-Z0-9_\[\]]+)\/(.+?)\}[\n\r\t]*/i",
		"/[\n\r\t]*\{date\((.+?)\)\}[\n\r\t]*/i",
		"/[\n\r\t]*\{avatar\((.+?)\)\}[\n\r\t]*/i",
		"/[\n\r\t]*\{eval\}\s*(\<\!\-\-)*(.+?)(\-\-\>)*\s*\{\/eval\}[\n\r\t]*/is",
		"/[\n\r\t]*\{eval\s+(.+?)\s*\}[\n\r\t]*/is",
		"/[\n\r\t]*\{csstemplate\}[\n\r\t]*/is",
		"/\{(\\\$[a-zA-Z0-9_\-\>\[\]\'\"\$\.\x7f-\xff]+)\s(or|\?\?)\s([a-zA-Z0-9\']+)\}/s",
		"/\{(\\\$[a-zA-Z0-9_\-\>\[\]\'\"\$\.\x7f-\xff]+)\}/s",
		"/\{hook\/(\w+?)(\s+(.+?))?\}/i",
		"/((\\\$[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*(\-\>)?[a-zA-Z0-9_\x7f-\xff]*)(\[[a-zA-Z0-9_\-\.\"\'\[\]\$\x7f-\xff]+\])*)/s",
		"/\<\?\=\<\?\=((\\\$[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*(\-\>)?[a-zA-Z0-9_\x7f-\xff]*)(\[[a-zA-Z0-9_\-\.\"\'\[\]\$\x7f-\xff]+\])*)\?\>\?\>/s",
		"/[\n\r\t]*\{template\s+([a-z0-9_:\/]+)\}[\n\r\t]*/is",
		"/[\n\r\t]*\{template\s+(.+?)\}[\n\r\t]*/is",
		"/[\n\r\t]*\{echo\s+(.+?)\}[\n\r\t]*/is",
		"/([\n\r\t]*)\{if\s+(.+?)\}([\n\r\t]*)/is",
		"/([\n\r\t]*)\{elseif\s+(.+?)\}([\n\r\t]*)/is",
		"/\{else\}/i",
		"/\{\/if\}/i",
		"/[\n\r\t]*\{loop\s+(\S+)\s+(\S+)\}[\n\r\t]*/is",
		"/[\n\r\t]*\{loop\s+(\S+)\s+(\S+)\s+(\S+)\}[\n\r\t]*/is",
		"/\{\/loop\}/i",
		"/[\n\r\t]*\{block\s+([a-zA-Z0-9_\[\]']+)\}(.+?)\{\/block\}/is",
	);
	private static $r_php = array(
		"/<\?/",
		"/\?>/",
	);
	private static $r_js = array(
		'/javascrit:/i',
		'/<script(.*?)>/i',
		'/\s+on([a-zA-Z]+)/i',
	);

	public static function getCells($dir) {
		static $files = null;
		if($directory = @dir($dir)) {
			while($entry = $directory->read()) {
				if($entry == '.' || $entry == '..') {
					continue;
				}
				$filename = $dir . '/' . $entry;
				if(is_file($filename)) {
					if(fileext($filename) == 'php') {
						$files[$filename] = $filename;
					}
				} else {
					self::getCells($filename);
				}
			}
			$directory->close();
		}
		return $files;
	}

	public static function getClass($file) {
		if(!file_exists($file)) {
			return null;
		}
		$p = strpos($file, 'cells/');
		if($p === false) {
			return null;
		}
		$cellId = substr(substr($file, $p + 6), 0, -4);
		$c = self::className($cellId);
		require_once $file;
		if(!property_exists($c, 'name') || !property_exists($c, 'cellList')) {
			return null;
		}
		return $cellId;
	}

	public static function className($s) {
		return str_replace('/', '_', $s);
	}

	public static function checkRequire($cellId, $template) {
		if(!self::className($cellId)::$requireList) {
			return true;
		}
		foreach(self::className($cellId)::$requireList as $cell) {
			if(!str_contains($template, '{cell ' . $cell . '}')) {
				return false;
			}
		}
		return true;
	}

	public static function checkTemplate($template) {
		$template = preg_replace("/\<\!\-\-\{(.+?)\}\-\-\>/s", "{\\1}", $template);
		foreach(self::$r_dzTpl as $r) {
			if(preg_match($r, $template)) {
				return false;
			}
		}
		foreach(self::$r_php as $r) {
			if(preg_match($r, $template)) {
				return false;
			}
		}
		foreach(self::$r_js as $r) {
			if(preg_match($r, $template)) {
				return false;
			}
		}
		return true;
	}

	public static function saveTemplate($id, $cellId, $type, $template) {
		global $_G;
		$_G['setting']['cells'][self::getTplKey($type)][$id][$cellId] = $template;
		$_G['setting']['cells'][self::getUsedKey($type)][$id][$cellId] = self::getUsedSetting($cellId, $template);

		$ids = array();
		foreach(C::t('common_style')->fetch_all_data(true) as $style) {
			$ids[] = $style['styleid'];
		}

		foreach($_G['setting']['cells'] as $mKey => $mVal) {
			foreach($mVal as $id => $data) {
				if(!in_array($id, $ids)) {
					unset($_G['setting']['cells'][$mKey][$id]);
				}
			}
		}

		$settings = [
			'cells' => $_G['setting']['cells'],
		];
		C::t('common_setting')->update_batch($settings);

		require_once libfile('function/cache');
		updatecache('setting');
		cleartemplatecache();
	}

	public static function getUsedSetting($cellId, $template) {
		if(!self::className($cellId)::$used) {
			return array();
		}

		$value = array();
		foreach(self::className($cellId)::$used as $cell => $key) {
			$value[$key] = str_contains($template, '{cell ' . $cell . '}') ? 1 : 0;
		}
		return $value;
	}

	public static function getTplKey($type) {
		return $type ? 'tplM' : 'tpl';
	}

	public static function getUsedKey($type) {
		return $type ? 'usedM' : 'used';
	}

	public static function getUsed($cellId) {
		global $_G;
		if(!empty($_ENV['cells'][$cellId])) {
			return $_ENV['cells'][$cellId];
		}
		$id = $_G['style']['styleid'];
		$usedKey = self::getUsedKey(defined('IN_MOBILE') ? 1 : 0);
		return $_G['setting']['cells'][$usedKey][$id][$cellId];
	}

	public static function getTemplate($cellId, $beginCell = '', $endCell = '') {
		global $_G;

		$type = defined('IN_MOBILE') ? 1 : 0;
		$tplKey = cells::getTplKey($type);
		$styleid = $_G['style']['styleid'];
		if(empty($_G['setting']['cells'][$tplKey][$styleid][$cellId])) {
			return '';
		}
		$template = $_G['setting']['cells'][$tplKey][$styleid][$cellId];
		if(!$beginCell && !$endCell) {
			return $template;
		}
		$beginCell = '{cell ' . $beginCell . '}';
		$endCell = '{cell ' . $endCell . '}';
		$p = strpos($template, $beginCell);
		if($p === false) {
			return '';
		}
		$template = substr($template, $p + strlen($beginCell));
		$p = strpos($template, $endCell);
		if($p === false) {
			return '';
		}
		return substr($template, 0, $p);
	}

}