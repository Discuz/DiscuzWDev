<?php

class i18n {

	const defaultPath = DISCUZ_ROOT.'./source/language/';

	public static function getFile($file) {
		global $_G;

		$return = self::defaultPath.$file;

		if($_G['i18n'] && !empty($_G['setting']['i18n'][$_G['i18n']]) &&
			is_dir($path = $_G['setting']['i18n'][$_G['i18n']].'/')) {
			if(file_exists($path.$file)) {
				$return = $path.$file;
			}
		}

		return $return;
	}

	public static function cmd($cmd, $langkey = '', $path = '') {
		global $_G;

		$i18n = !empty($_G['setting']['i18n']) ? $_G['setting']['i18n'] : array();

		switch ($cmd) {
			case 'get':
				return $i18n;
			case 'set':
				$i18n[$langkey] = $path;
				break;
			case 'rm':
				unset($i18n[$langkey]);
				break;
		}

		C::t('common_setting')->update_batch(array('i18n' => $i18n));
		require_once libfile('function/cache');
		updatecache('setting');

		$_G['setting']['i18n'] = $i18n;
		return '';
	}

}