<?php

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once libfile('function/post');

class cells_account_icons
{

	const Interfaces = array('wechat', 'qq', 'phone', 'douyin', 'workwx', 'dingtalk', 'feishu');

	const Interfaces_iconId = array(
	    'wechat' => 'icon-weixin',
	    'workwx' => 'icon-qiyeweixin',
	    'dingtalk' => 'icon-dingding01',
	    'feishu' => 'icon-feishu3',
	    'qq' => 'icon-social-qq',
	    'phone' => 'icon-shoujiduanxin',
	    'douyin' => 'icon-douyin',
	);

	public static function process()
	{
		global $_G;
		foreach (self::Interfaces as $interface) {
			$_G['account_icons'][] = array($interface, lang('admincp_menu', 'menu_setting_' . $interface), self::Interfaces_iconId[$interface]);
		}
	}

}