<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class tplfile {

	public static function get_content($templateid, $file) {
		global $_G;

		static $cache = array();
		if(empty($_G['cache']['tplfile_' . $templateid])) {
			if(empty($cache[$templateid])) {
				loadcache('tplfile_' . $templateid);
				$cache[$templateid] = true;
			}
		}
		if(empty($_G['cache']['tplfile_' . $templateid])) {
			return false;
		}
		if(empty($_G['cache']['tplfile_' . $templateid][$file])) {
			return false;
		}
		return $_G['cache']['tplfile_' . $templateid][$file];
	}

	public static function file_exists($file) {
		global $_G;

		loadcache('tpldir_index');

		$cutFile = str_replace(array(DISCUZ_ROOT, './'), '', $file);
		$return = false;
		foreach($_G['cache']['tpldir_index'] as $tpldir => $templateid) {
			$p = strlen($tpldir) + 1;
			if(substr($cutFile, 0, $p) == $tpldir . '/') {
				$f = substr($cutFile, $p - 1);
				$return = self::get_content($templateid, $f) !== false;
			}
		}
		if($return) {
			return 2;
		}
		return file_exists($file) ? 1 : 0;
	}

	public static function file_get_contents($file) {
		global $_G;

		loadcache('tpldir_index');

		$cutFile = str_replace(array(DISCUZ_ROOT, './'), '', $file);
		foreach($_G['cache']['tpldir_index'] as $tpldir => $templateid) {
			$p = strlen($tpldir) + 1;
			if(substr($cutFile, 0, $p) == $tpldir . '/') {
				$f = substr($cutFile, $p - 1);
				$data = self::get_content($templateid, $f);
				if($data !== false) {
					return $data;
				}
			}
		}
		return @implode('', file($file));
	}

}