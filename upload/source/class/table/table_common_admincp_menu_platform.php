<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_admincp_cmenu.php 27806 2012-02-15 03:20:46Z svn_project_zhangjie $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_admincp_menu_platform extends discuz_table
{
	public function __construct() {

		$this->_table = 'common_admincp_menu_platform';
		$this->_pk    = 'platform';

		parent::__construct();
	}

	public function fetch_all_data() {
		return DB::fetch_all("SELECT * FROM %t", array($this->_table), 'platform');
	}

}

?>