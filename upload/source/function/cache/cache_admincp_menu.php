<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cache_admingroups.php 24830 2011-10-12 08:23:34Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function build_cache_admincp_menu() {
	$subperms = array();
	foreach(C::t('common_admincp_menu_platform')->fetch_all_data() as $data) {
		$menuData = dunserialize($data['menu']);
		foreach($menuData['menu'] as $topmenu => $submenu) {
			foreach($submenu as $row) {
				if(!empty($row[5])) {
					foreach($row[5] as $perm) {
						$subperms[$perm] = $row[1];
					}
				}
			}
		}
	}
	savecache('menu_subperms', $subperms);
}

?>