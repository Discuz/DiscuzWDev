<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

define('DISCUZ_DATA', DISCUZ_ROOT.'data/');
define('DISCUZ_ROOT_STATIC', DISCUZ_ROOT);
define('PLUGIN_ROOT', './source/plugin/');
define('TEMPLATE_ROOT', './template/');

function DISCUZ_ROOT(){
	if(defined('DISCUZ_ROOT')) {
		return DISCUZ_ROOT;
	} else {
		return substr(dirname(__FILE__), 0, -15);
	}
}
function DISCUZ_PLUGIN($plugin = '') {
	return DISCUZ_ROOT().PLUGIN_ROOT.$plugin;
}

function DISCUZ_TEMPLATE($template = '') {
	return DISCUZ_ROOT().TEMPLATE_ROOT.$template;
}