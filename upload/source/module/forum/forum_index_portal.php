<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: forum_index.php 36265 2016-11-04 07:10:47Z nemohou $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

if($_G['setting']['domain']['defaultindex'] == 'forum.php?mod=forumdisplay&fid=0') {
	$uriBase = 'index.php?';
} else {
	$uriBase = 'forum.php?mod=forumdisplay&fid=0&';
}

if(empty($_G['setting']['forumportal']['navList']) || !is_array($_G['setting']['forumportal']['navList'])) {
	showmessage('forumportal_no_setting');
}

$tpp = !empty($_G['setting']['forumportal']['setting']['tpp']) ? $_G['setting']['forumportal']['setting']['tpp'] : $_G['tpp'];

//获取可见的导航列表
$portalNavList = array();
foreach($_G['setting']['forumportal']['navList'] as $navId => $row) {
	if(!$row['allow']) {
		continue;
	}
	if(!empty($row['adminid']) && $row['adminid'] > 0 && (!$_G['adminid'] || $_G['adminid'] > $row['adminid'])) {
		continue;
	}
	$portalNavList[$navId] = $row;
}

if(!$portalNavList) {
	showmessage('forumportal_no_setting');
}

//获取当前的navId
if(!isset($_GET['navId'])) {
	$curNavId = array_keys($portalNavList)[0];
} else {
	if(!isset($portalNavList[$_GET['navId']])) {
		showmessage('forumportal_page_not_found');
	}
	$curNavId = $_GET['navId'];
}
$setting = $_G['setting']['forumportal']['navList'][$curNavId];
if($_GET['debug']) {
	debug($setting);
}

//生成过滤参数
$filterarr = array();

if(!empty($setting['forum_fids'])) {
	$filterarr['inforum'] = $setting['forum_fids'];
}
if(!empty($setting['group_fids'])) {
	$filterarr['inforum'] = array_merge($filterarr['inforum'], explode(',', $setting['group_fids']));
}
if(!empty($setting['authorids'])) {
	$filterarr['authorid'] = explode(',', $setting['authorids']);
	$filterarr['noanony'] = true;
}
if(!empty($setting['digest'])) {
	$filterarr['digest'] = range(1, $setting['digest']);
}
if(!empty($setting['displayorder'])) {
	$filterarr['sticky'] = 4;
	$filterarr['displayorder'] = range(1, $setting['displayorder']);
} else {
	$filterarr['sticky'] = 0;
}
if(!empty($setting['special'])) {
	$filterarr['specialthread'] = 1;
	$filterarr['special'] = $setting['special'];
}
if(!empty($setting['heats'])) {
	$filterarr['heats'] = $setting['heats'];
}
if(!empty($setting['recommends'])) {
	$filterarr['recommends'] = $setting['recommends'];
}
if(!empty($setting['dateline'])) {
	$filterarr['starttime'] = dgmdate(TIMESTAMP - $setting['dateline']);
}
if(!empty($setting['laspost'])) {
	$filterarr['lastpostmore'] = TIMESTAMP - $setting['laspost'];
}

//生成排序规则
if(!empty($setting['order'])) {
	$field = 'lastpost';
	switch($setting['order']) {
		case 1:
			$field = 'dateline';
			break;
		case 2:
			$field = 'replies';
			break;
		case 3:
			$field = 'views';
			break;
		case 4:
			$field = 'heats';
			break;
		case 5:
			$field = 'recommends';
			break;
	}
	$order = "$field DESC";
} else {
	$order = "lastpost DESC";
}

//查询
$_G['forum_threadcount'] = C::t('forum_thread')->count_search($filterarr, 0);
$_G['forum_threadimage'] = !empty($_G['setting']['forumportal']['setting']['image']) ? $_G['setting']['forumportal']['setting']['image'] : array();

$page = max(1, $_G['page']);
$page = $_G['setting']['threadmaxpages'] && $page > $_G['setting']['threadmaxpages'] ? 1 : $page;
$maxPage = @ceil($_G['forum_threadcount'] / $tpp);
$page = $maxPage < $page ? 1 : $page;
$start_limit = ($page - 1) * $tpp;
$nextpage = '';

$threadlist = C::t('forum_thread')->fetch_all_search($filterarr, 0, $start_limit, $tpp, $order, '');

$_G['ppp'] = $_G['forum']['threadcaches'] && !$_G['uid'] ? $_G['setting']['postperpage'] : $_G['ppp'];

$used = cells::getUsed('forum/portal/threadlist');
if(!empty($used['nextpage'])) {
	$_GET['ajaxtarget'] = 'threadlistAppend';
	$nextpage = $tpp == count($threadlist) ? $uriBase.'navId='.$curNavId.'&page='.($page + 1) : '';
} else {
	$_GET['ajaxtarget'] = 'threadlist';
	$multipage = multi($_G['forum_threadcount'], $tpp, $page, $uriBase . 'navId=' . $curNavId, $_G['setting']['threadmaxpages']);
}

$page = $_G['page'];

$allowleftside = 0;
$allowside = 1;

$leftside = empty($_G['cookie']['disableleftside']) && $allowleftside ? forumleftside() : array();

if($_G['inajax']) {
	include template('forum/ajax_index_portal');
} else {
	include template('diy:forum/index_portal');
}