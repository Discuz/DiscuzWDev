<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: spacecp_credit.php 32023 2012-10-31 08:20:37Z liulanbo $
 */

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}
global $_G;

$list = array();
foreach (cells_account_icons::Interfaces as $interface) {
	$list[] = array($interface, lang('admincp_menu', 'menu_setting_' . $interface), cells_account_icons::Interfaces_iconId[$interface]);
}

if(!empty($_GET['op'])) {
	showmessage('此为开发者版，无实际功能');
}

$_G['setting']['security_rename'] = $_G['setting']['security_password'] = $_G['setting']['security_question'] = $_G['setting']['security_email'] = $_G['setting']['security_mobile'] = $_G['setting']['security_logoff'] = true;
$_G['member']['loginname'] = $_G['member']['username'].'_login';

$account_list = array(
    'wechat' => array(
	'account' => 'account1',
	'create_time' => dgmdate(time()),
	'bindname' => '账号1',
    ),
    'qq' => array(
	'account' => 'account2',
	'create_time' => dgmdate(time()),
	'bindname' => '账号2',
    )
);

include template('home/spacecp_account');

?>