<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: admincp_menu.php 36284 2016-12-12 00:47:50Z nemohou $
 */

global $_G;
if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

$isfounder = isset($isfounder) ? $isfounder : isfounder();

loaducenter();

$menuData = array();

class menu_loader {

	public static function run(&$menuData) {
		global $_G;

		if(PLATFORM == 'system' && (empty($_G['cache']['menu_platform_'.PLATFORM]) || !empty($_GET['resetmenu']))) {
			menu::platform_add('system', self::_initData(), true);
		}
		$menuData = $_G['cache']['menu_platform_'.PLATFORM];
		foreach($menuData['menu'] as $key => $submenu) {
			foreach($submenu as $k => $row) {
				if(!empty($row[3])) {
					list($s1, $s2) = explode('::', $row[3]);
					if(!$s2) {
						if(method_exists(__CLASS__, $row[3])) {
							if(!self::{$row[3]}()) {
								unset($submenu[$k]);
							}
						} elseif(empty($GLOBALS['_G']['setting'][$row[3]])) {
							unset($submenu[$k]);
						}
					} else {
						$f = DISCUZ_ROOT.'./source/plugin/'.$s1.'/platform.class.php';
						if(file_exists($f)) {
							require_once $f;
							$c = 'platform_'.$s1;
							if(method_exists($c, $s2) && !$c::$s2()) {
								unset($submenu[$k]);
							}
						}
					}
				} elseif(!empty($row[4])) {
					list($s1, $s2) = explode('::', $row[4]);
					if(!$s2) {
						if(method_exists(__CLASS__, $row[4])) {
							$v = self::{$row[4]}();
							array_splice($submenu, $k, 0, $v);
						}
					} else {
						$f = DISCUZ_ROOT.'./source/plugin/'.$s1.'/platform.class.php';
						if(file_exists($f)) {
							require_once $f;
							$c = 'platform_'.$s1;
							if(method_exists($c, $s2)) {
								$v = $c::$s2();
								array_splice($submenu, $k, 0, $v);
							}
						}
					}
				}
			}
			$menuData['menu'][$key] = $submenu;
		}
		$replaces = array(
			'{ADMINSCRIPT}' => ADMINSCRIPT,
		);
		foreach($replaces as $k => $v) {
			$menuData['logo'] = str_replace($k, $v, $menuData['logo']);
			$menuData['navbar'] = str_replace($k, $v, $menuData['navbar']);
		}
	}

	public static function homestatus() {
		global $_G;

		return $_G['setting']['doingstatus'] || $_G['setting']['blogstatus'] ||
			$_G['setting']['feedstatus'] || $_G['setting']['albumstatus'] ||
			$_G['setting']['wallstatus'] || $_G['setting']['sharestatus'];
	}

	public static function isfounder() {
		return $GLOBALS['isfounder'];
	}

	public static function isdeveloper() {
		global $_G;
		return $GLOBALS['isfounder'] && isset($_G['config']['plugindeveloper']) && $_G['config']['plugindeveloper'] > 0;
	}

	public static function isdebug() {
		return defined('DISCUZ_DEBUG') && DISCUZ_DEBUG && $GLOBALS['isfounder'];
	}

	public static function uc_standalone() {
		return $GLOBALS['isfounder'] && !UC_STANDALONE;
	}

	public static function verifyList() {
		global $_G;

		$menu = array();
		if(is_array($_G['setting']['verify'])) {
			foreach($_G['setting']['verify'] as $vid => $verify) {
				if($vid != 7 && $verify['available']) {
					$menu[] = array($verify['title'], "verify_verify_$vid");
				}
			}
		}

		return $menu;
	}

	public static function pluginList() {
		global $_G;

		$menu = array();
		loadcache('adminmenu');
		if(is_array($_G['cache']['adminmenu'])) {
			foreach($_G['cache']['adminmenu'] as $row) {
				if($row['name'] == 'plugins_system') {
					$row['name'] = cplang('plugins_system');
				}
				$menu[] = array($row['name'], $row['action'], $row['sub']);
			}
		}

		return $menu;
	}

	public static function customMenuList() {
		return get_custommenu();
	}

	private static function _initData() {

		$logo = '<a href="{ADMINSCRIPT}?frames=yes&action=index" class="logo"><img src="static/image/admincp/logo.svg" alt="Discuz! Administrator\'s Control Panel"></a>';

		$navbar = '
<form name="search" method="post" autocomplete="off" action="{ADMINSCRIPT}?action=search" target="main">
	<input type="text" name="keywords" value="" class="txt" required>
	<button type="submit" name="searchsubmit" value="yes" class="btn"></button>
</form>
';

		$menu = array();

		/**
		 * array(
		 *      'menu_setting_optimize',            // 菜单标题文本
		 *      'setting_cachethread',              // 菜单 Key，系统: [action]_[operation]_[do]，插件: plugin_[identifier]_[pmod]，URL: 跳转链接
		 *      0,                                  // 0: 无，1: 分割区域开始，2: 分割区域结束，_blank: 新窗口打开
		 *      '',                                 // $_G['setting']['xxx']、menu_loader::xxx、plugin::xxx 返回为真时显示菜单
		 *      '',                                 // 将 menu_loader::xxx、plugin::xxx 返回的菜单列表替换此条菜单
		 *      array('setting_serveropti')         // 此菜单项目包含的其他菜单 Key，用于权限
		 * )
		 */

		$menu['index'] = array(
			array('menu_home', 'index'),
			array('menu_custommenu_manage', 'misc_custommenu'),
			array('menu_setting_runtests', 'runtests', 0, 'isdebug'),
			array('', '', 0, '', 'customMenuList'),
		);

		$menu['global'] = array(
			array('menu_setting_basic', 'setting_basic'),
			array('menu_setting_access', 'setting_access'),
			array('menu_setting_functions', 'setting_functions'),
			array('menu_setting_optimize', 'setting_cachethread', 0, '', '', array('setting_serveropti', 'setting_memory', 'setting_memorydata')),
			array('menu_setting_seo', 'setting_seo'),
			array('menu_setting_domain', 'domain'),
			array('menu_setting_follow', 'setting_follow', 0, 'followstatus'),
			array('menu_setting_home', 'setting_home'),
			array('menu_setting_user', 'setting_permissions'),
			array('menu_setting_credits', 'setting_credits', 0, '', '', array('credits_list')),
			array('menu_setting_datetime', 'setting_datetime'),
			array('menu_setting_attachments', 'setting_attach'),
			array('menu_setting_imgwater', 'setting_imgwater', 0, '', '', array('checktools_imagepreview')),
			array('menu_posting_attachtypes', 'misc_attachtype'),
			array('menu_setting_search', 'setting_search'),
			array('menu_setting_district', 'district'),
			array('menu_setting_ranklist', 'setting_ranklist', 0, 'rankliststatus'),
			array('menu_setting_mobile', 'setting_mobile'),
			array('menu_setting_antitheft', 'setting_antitheft'),
		);

		$menu['style'] = array(
			array('menu_setting_customnav', 'nav'),
			array('menu_setting_styles', 'setting_styles', 0, '', '', array('setting_threadprofile')),
			array('menu_posting_smilies', 'smilies'),
			array('menu_click', 'click'),
			array('menu_thread_stamp', 'misc_stamp'),
			array('menu_posting_editor', 'setting_editor', 0, '', '', array('misc_bbcode', 'editorblock')),
			array('menu_misc_onlinelist', 'misc_onlinelist'),
		);

		$menu['topic'] = array(
			array('menu_moderate_posts', 'moderate'),
			array('menu_remoderate', 'remoderate'),
			array('menu_posting_censors', 'misc_censor'),
			array('menu_maint_report', 'report'),
			array('menu_setting_tag', 'tag'),
			array('menu_setting_collection', 'collection', 0, 'collectionstatus'),
			array(cplang('nav_forum'), '', 1),
			array('menu_maint_threads', 'threads'),
			array('menu_maint_prune', 'prune'),
			array('menu_maint_attaches', 'attach'),
			array(cplang('nav_forum'), '', 2),
			array(cplang('nav_group'), '', 1, 'groupstatus'),
			array('menu_maint_threads_group', 'threads_group', 0, 'groupstatus'),
			array('menu_maint_prune_group', 'prune_group', 0, 'groupstatus'),
			array('menu_maint_attaches_group', 'attach_group', 0, 'groupstatus'),
			array(cplang('nav_group'), '', 2, 'groupstatus'),
			array(cplang('thread'), '', 1),
			array('menu_moderate_recyclebin', 'recyclebin'),
			array('menu_moderate_recyclebinpost', 'recyclebinpost'),
			array('menu_threads_forumstick', 'threads_forumstick'),
			array('menu_postcomment', 'postcomment'),
			array(cplang('thread'), '', 2),
			array(cplang('nav_home'), '', 1, 'homestatus'),
			array('menu_maint_doing', 'doing', 0, 'doingstatus'),
			array('menu_maint_blog', 'blog', 0, 'blogstatus'),
			array('menu_maint_blog_recycle_bin', 'blogrecyclebin', 0, 'blogstatus'),
			array('menu_maint_feed', 'feed', 0, 'feedstatus'),
			array('menu_maint_album', 'album', 0, 'albumstatus'),
			array('menu_maint_pic', 'pic', 0, 'albumstatus'),
			array('menu_maint_comment', 'comment', 0, 'wallstatus'),
			array('menu_maint_share', 'share', 0, 'sharestatus'),
			array(cplang('nav_home'), '', 2, 'homestatus'),
		);

		$menu['user'] = array(
			array('menu_members_edit', 'members_search', 0, '', '', array('members_clean', 'members_repeat')),
			array('menu_members_add', 'members_add'),
			array('menu_members_profile', 'members_profile', 0, '', '', array('setting_profile')),
			array('menu_members_stat', 'members_stat'),
			array('menu_usertag', 'usertag'),
			array('menu_members_edit_ban_user', 'members_ban'),
			array('menu_members_ipban', 'members_ipban'),
			array('menu_members_credits', 'members_reward'),
			array('menu_moderate_modmembers', 'moderate_members'),
			array('menu_follow', 'specialuser_follow'),
			array('menu_defaultuser', 'specialuser_defaultuser'),
			array('menu_members_verify_profile', 'verify_verify'),
			array('menu_members_verify_setting', 'verify'),
			array('', '', 0, '', 'verifyList'),
			array(cplang('usergroup'), '', 1),
			array('menu_usergroups', 'usergroups'),
			array('menu_admingroups', 'admingroup'),
			array(cplang('usergroup'), '', 2),
			array(cplang('multisend'), '', 1),
			array('menu_members_newsletter', 'members_newsletter', 0, '', '', array('members_grouppmlist')),
			array('menu_members_mobile', 'members_newsletter_mobile'),
			array('menu_members_sms', 'members_newsletter_sms'),
			array(cplang('multisend'), '', 2),
		);

		$menu['portal'] = array(
			array('menu_portalcategory', 'portalcategory'),
			array('menu_article', 'article'),
			array('menu_topic', 'topic'),
			array('menu_html', 'makehtml'),
	array('menu_diytemplate', 'diytemplate'),
	array('menu_block', 'block'),
	array('menu_blockstyle', 'blockstyle'),
	array('menu_blockxml', 'blockxml'),
	array('menu_portalpermission', 'portalpermission'),
	array('menu_blogcategory', 'blogcategory'),
	array('menu_albumcategory', 'albumcategory'),
);

$menu['forum'] = array(
	array('menu_forums', 'forums'),
	array('menu_forums_merge', 'forums_merge'),
	array('menu_forums_infotypes', 'threadtypes'),
			array('menu_grid', 'grid'),
			array('menu_forums_portal', 'forumportal'),
		);

		$menu['group'] = array(
			array('menu_group_setting', 'group_setting'),
			array('menu_group_type', 'group_type', 0, '', '', array('group_mergetype', 'group_editgroup')),
			array('menu_group_manage', 'group_manage'),
			array('menu_group_userperm', 'group_userperm'),
			array('menu_group_level', 'group_level'),
			array('menu_group_mod', 'group_mod'),
		);

		$menu['safe'] = array(
			array('menu_safe_setting', 'setting_sec'),
			array('menu_safe_seccheck', 'setting_seccheck'),
			array('menu_security', 'optimizer_security', 0, 'isfounder'),
			array('menu_serversec', 'optimizer_serversec', 0, 'isfounder'),
			array('menu_safe_accountguard', 'setting_accountguard'),
		);

		$menu['extended'] = array(
			array('menu_misc_announce', 'announce'),
			array('menu_adv_custom', 'adv'),
			array('menu_tasks', 'tasks', 0, 'taskstatus'),
			array('menu_magics', 'magics', 0, 'magicstatus', '', array('members_confermagic')),
			array('menu_medals', 'medals', 0, 'medalstatus', '', array('members_confermedal')),
			array('menu_smsgw', 'smsgw', 0, 'isfounder'),
			array('menu_misc_help', 'faq'),
			array('menu_ec', 'setting_ec', 0, '', '', array('ec', 'tradelog')),
			array('menu_misc_link', 'misc_link'),
			array('memu_focus_topic', 'misc_focus'),
			array('menu_misc_relatedlink', 'misc_relatedlink'),
			array('menu_card', 'card')
		);

		$menu['plugin'] = array(
			array('menu_plugins', 'plugins', 0, 'isfounder'),
			array('', '', 0, '', 'pluginList'),
		);

		$menu['template'] = array(
			array('menu_styles', 'styles', 0, '', '', array('cells')),
			array('menu_templates_add', 'templates_add', 0, 'isdeveloper'),
		);

		$menu['tools'] = array(
			array('menu_tools_updatecaches', 'tools_updatecache'),
			array('menu_tools_updatecounters', 'counter'),
			array('menu_logs', 'logs'),
			array('menu_misc_cron', 'misc_cron'),
			array('menu_tools_fileperms', 'tools_fileperms', 0, 'isfounder'),
			array('menu_tools_filecheck', 'checktools_filecheck', 0, 'isfounder'),
			array('menu_tools_hookcheck', 'checktools_hookcheck', 0, 'isfounder'),
			array('menu_tools_replacekey', 'checktools_replacekey', 0, 'isfounder'),
		);

		$menu['founder'] = array(
			array('menu_founder_perm', 'founder_perm', 0, 'isfounder'),
			array('menu_platform', 'founder_platform', 0, 'isfounder'),
			array('menu_setting_mail', 'setting_mail'),		
			array('menu_setting_uc', 'setting_uc', 0, 'isfounder'),
			array('menu_db', 'db_export', 0, 'isfounder'),
			array('menu_membersplit', 'membersplit_check', 0, 'isfounder'),
			array('menu_postsplit', 'postsplit_manage', 0, 'isfounder'),
			array('menu_threadsplit', 'threadsplit_manage', 0, 'isfounder'),
			array('menu_optimizer', 'optimizer_performance', 0, 'isfounder'),
		);

		$menu['cloudaddons'] = array(
			array('menu_addons', 'cloudaddons&frame=no', '_blank', 'isfounder'),
		);

		$menu['uc'] = array(
			array('header_uc', '{UCAPI}/admin.php?m=frame', '_blank', 'uc_standalone'),
		);

		return array(
			'name' => cplang('platform_system'),
			'title' => cplang('admincp_title'),
			'framecss' => '',
			'pagecss' => '',
			'logo' => $logo,
			'navbar' => $navbar,
			'menu' => $menu,
		);
	}
}

menu_loader::run($menuData);

$menu = &$menuData['menu'];
foreach($menu as $top => $v) {
	if(empty($v)) {
		continue;
	}
	$topmenu[$top] = '';
}

if(!$isfounder && !isset($GLOBALS['admincp']->perms['all'])) {
	$menunew = $menu;
	foreach($menu as $topkey => $datas) {
		if($topkey == 'index') {
			continue;
		}
		$itemexists = 0;
		foreach($datas as $key => $data) {
			if(array_key_exists($data[1], $GLOBALS['admincp']->perms)) {
				$itemexists = 1;
			} else {
				unset($menunew[$topkey][$key]);
			}
		}
		if(!$itemexists) {
			unset($topmenu[$topkey]);
			unset($menunew[$topkey]);
		}
	}
	$menu = $menunew;
}

?>