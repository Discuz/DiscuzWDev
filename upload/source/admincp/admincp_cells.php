<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: admincp_styles.php 36353 2017-01-17 07:19:28Z nemohou $
 */

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

cpheader();

echo <<<EOF
<style>
.itemtitle ul li>a { padding: 5px; margin-bottom: 2px; }
</style>
EOF;

shownav('style', 'cells');

$id = $_GET['id'] ?? $_G['style']['styleid'];
$cellId = $_GET['cellId'] ?? '';
if(!preg_match('/^[\/\w_-]+$/', $cellId)) {
	$cellId = '';
}

$style = C::t('common_style')->fetch_by_styleid($id);
if(!$style) {
	cpmsg('style_not_found', '', 'error');
}

if(!$cellId) {
	$files = cells::getCells($style['directory'].'/cells');

	if(!$files) {
		cpmsg('cells_not_found', '', 'error');
	}

	showsubmenu('cells');
	showtableheader($style['name']);
	foreach($files as $file) {
		$cellId = cells::getClass($file);
		if($cellId) {
			echo '<tr><td><a class="files bold" href="'.ADMINSCRIPT.'?action=cells&id='.$id.'&cellId='.$cellId.'">'.cells::className($cellId)::$name.'</a></td></tr>';
		}
	}
	showtablefooter();
} else {
	$type = !empty($_GET['type']) ? 1 : 0;
	$_cellId = cells::getClass($style['directory'].'/cells/'.$cellId.'.php');
	if(!$_cellId) {
		if($id != 1) {
			$id = 1;
			$style = C::t('common_style')->fetch_by_styleid($id);
			if(!$style) {
				frame_cpmsg('style_not_found');
			}
			$_cellId = cells::getClass($style['directory'].'/cells/'.$cellId.'.php');
			if(!$_cellId) {
				frame_cpmsg('cells_not_found');
			}
		} else {
			frame_cpmsg('cells_not_found');
		}
	}
	$cellId = $_cellId;
	$c = cells::className($cellId);

	if(submitcheck('submit')) {
		if(strlen($_GET['cell']) == 0 || !empty($_GET['del'])) {
			$_GET['cell'] = $c::getDefault($type);
		}
		if(!cells::checkRequire($cellId, $_GET['cell'])) {
			frame_cpmsg('cells_require_error');
		}
		if(!cells::checkTemplate($_GET['cell'])) {
			frame_cpmsg('cells_format_error');
		}
		cells::saveTemplate($id, $cellId, $type, $_GET['cell']);
		frame_cpmsg('cells_edit_succeed', true);
	}

	$list = '';
	foreach($c::$cellList as $cell => $memo) {
		$list .= '<li><a href="javascript:insertunit($(\'cell\'), \'{cell '.$cell.'}\')">'.$memo.'</a></li>';
	}

	loadcache('styleconsts');
	if(!empty($_G['cache']['styleconsts']) && !empty($_G['cache']['styleconsts'][$id])) {
		$list .= '<li><select onchange="insertunit($(\'cell\'), this.value)"><option value="">- '.cplang('cells_select_styleconsts').' -</option>';
		foreach($_G['cache']['styleconsts'][$id] as $k => $v) {
			$list .= '<option value="'.$k.'">'.$k.'</option>';
		}
		$list .= '</select>';
	}

	showsubmenu('cells');
	$value = $_G['setting']['cells'][cells::getTplKey($type)][$id][$cellId] ?: $c::getDefault($type);
	showformheader('cells&id='.$id.'&cellId='.$cellId.'&type='.$type, 'target="hframe"');
	showtableheader('<a href="'.ADMINSCRIPT.'?action=cells&id='.$id.'">'.$style['name'].'</a> &raquo; '.$c::$name, 'tb2');
	echo '<tr style="height: 600px">';
	echo '<td valign="top" width="70%">';
	echo '<div class="itemtitle">';
	echo '<ul class="tab1">';
	echo '<li'.(!$type ? ' class="current"' : '').'><a href="'.ADMINSCRIPT.'?action=cells&id='.$id.'&cellId='.$cellId.'&type=0"><span>PC</span></a></li>';
	echo '<li'.($type ? ' class="current"' : '').'><a href="'.ADMINSCRIPT.'?action=cells&id='.$id.'&cellId='.$cellId.'&type=1"><span>Mobile</span></a></li>';
	echo '</ul></div>';
	echo '<textarea id="cell" style="width:98%;height: 90%" name="cell" spellcheck="false">'.dhtmlspecialchars($value).'</textarea></td>';
	echo '<td valign="top" class="tipsblock"><div class="infotitle1">'.cplang('cell_item').'</div><ul>'.$list.'</ul><br /><div class="infotitle1">'.cplang('cell_usage').'</div>'.$c::$useage.'</td>';
	echo '</tr>';
	showsubmit('submit', 'submit', '',
		'<label><input name="del" class="checkbox" value="1" type="checkbox" />'.cplang('to_default').'</label> &nbsp; '.cplang('cells_notice'));
	showtablefooter();
	showformfooter();
	echo '<iframe id="hframe" name="hframe" style="display: none"></iframe>';
}