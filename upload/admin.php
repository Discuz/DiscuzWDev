<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: admin.php 34285 2013-12-13 03:39:35Z hypowang $
 */

$_platform_ = 'system';
if(!empty($_GET['platform'])) {
	$_p = strpos($_GET['platform'], '?');
	if($_p !== false) {
		parse_str(substr($_GET['platform'], $_p + 1), $_get);
		$_platform_ = substr($_GET['platform'], 0, $_p);
		$_GET += $_get;
		$_p = strpos($_SERVER['QUERY_STRING'], '?');
		$_SERVER['QUERY_STRING'] = substr($_SERVER['QUERY_STRING'], $_p + 1);
	} else {
		$_platform_ = $_GET['platform'];
		$_SERVER['QUERY_STRING'] = '';
	}
	unset($_GET['platform']);
}

define('PLATFORM', $_platform_);
define('IN_ADMINCP', TRUE);
define('NOROBOT', TRUE);
define('ADMINSCRIPT', basename(__FILE__).(PLATFORM != '' ? '?platform='.PLATFORM : ''));
define('CURSCRIPT', 'admin');
define('HOOKTYPE', 'hookscript');
define('APPTYPEID', 0);


require './source/class/class_core.php';
require './source/function/function_misc.php';
require './source/function/function_forum.php';
require './source/function/function_admincp.php';
require './source/function/function_cache.php';

$discuz = C::app();
$discuz->init_cron = false;
C::app()->cachelist = array('menu_platform_'.PLATFORM);
$discuz->init();

$admincp = new discuz_admincp();
$admincp->core  = & $discuz;
$admincp->init();

$action = preg_replace('/[^\[A-Za-z0-9_\]]/', '', getgpc('action'));
$operation = preg_replace('/[^\[A-Za-z0-9_\]]/', '', getgpc('operation'));
$do = preg_replace('/[^\[A-Za-z0-9_\]]/', '', getgpc('do'));
$frames = preg_replace('/[^\[A-Za-z0-9_\]]/', '', getgpc('frames'));
lang('admincp');
$lang = & $_G['lang']['admincp'];
$page = max(1, intval(getgpc('page')));
$isfounder = $admincp->isfounder;

if(empty($action) || $frames != null) {
	$admincp->show_admincp_main();
} elseif($action == 'logout') {
	$admincp->do_admin_logout();
	dheader("Location: ./index.php");
} elseif(($admincp->allow($action, $operation, $do) || $action == 'index') && ($f = $admincp->admincpfile($action))) {
	require_once $f;
} else {
	cpheader();
	if($action == 'cloudaddons') {
		cpmsg('cloudaddons_noaccess', '', 'error');
	} else {
		cpmsg('action_noaccess', '', 'error');
	}
}
?>