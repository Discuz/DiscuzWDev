DROP TABLE IF EXISTS pre_common_admincp_menu_platform;
CREATE TABLE pre_common_admincp_menu_platform
(
    platform varchar(255) NOT NULL DEFAULT 'system',
    menu     text NOT NULL COMMENT '菜单内容',
    PRIMARY KEY (platform)
) ENGINE = InnoDB;