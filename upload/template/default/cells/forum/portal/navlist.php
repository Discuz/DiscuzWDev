<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/*
 * 聚合主题导航的细胞模板
 * 模板调用方法：{cells forum/portal/navlist}
 */

class forum_portal_navlist {

	public static $name = '聚合首页导航';
	public static $useage = '{cells forum/portal/navlist}';
	public static $cellList = array(
		'forum/portal/navlist/loop_start' => '循环体开始 (必须包含)',
		'forum/portal/navlist/loop_end' => '循环体结束 (必须包含)',
		'forum/portal/navlist/name' => '导航文字',
		'forum/portal/navlist/url' => '导航链接',
		'forum/portal/navlist/current_class' => '当前导航样式',
	);
	public static $requireList = array(
		'forum/portal/navlist/loop_start',
		'forum/portal/navlist/loop_end',
	);

	public static $used = array();

	public static function getDefault($type = 0) {
		if(!$type) {
			return <<<EOF
<ul class="ttp cl">
{cell forum/portal/navlist/loop_start}
<li{cell forum/portal/navlist/current_class}><a href="{cell forum/portal/navlist/url}" ajaxtarget="threadlist">{cell forum/portal/navlist/name}</a></li>
{cell forum/portal/navlist/loop_end}
</ul>
EOF;
		} else {
			return <<<EOF
<div class="dhnav_box"><div id="dhnav"><div id="dhnav_li"><ul class="flex-box">
{cell forum/portal/navlist/loop_start}
<li{cell forum/portal/navlist/current_class}><a href="{cell forum/portal/navlist/url}" ajaxtarget="threadlist">{cell forum/portal/navlist/name}</a></li>
{cell forum/portal/navlist/loop_end}
</ul></div></div></div>
EOF;
		}
	}

}



